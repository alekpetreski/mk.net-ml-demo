﻿using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class ApplicationFlow : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Cube to show when an OK gesture is recognized.")]
    private GameObject _cube;

    private MLHandKeyPose[] _gestures;

    private void Awake()
    {
        _cube.SetActive(false);

        _gestures = new[]
        {
            MLHandKeyPose.L,
            MLHandKeyPose.Ok,
            MLHandKeyPose.Fist,
            MLHandKeyPose.Pinch
        };
    }

    private void Start()
    {
        if (!MLHands.IsStarted)
        {
            MLHands.Start();
        }

        MLHands.KeyPoseManager.EnableKeyPoses(_gestures, true);
    }

    private void Update()
    {
        if (MLHands.Left.KeyPose == MLHandKeyPose.Ok && MLHands.Left.KeyPoseConfidence > 0.8f ||
            MLHands.Right.KeyPose == MLHandKeyPose.Ok && MLHands.Right.KeyPoseConfidence > 0.8f)
        {
            _cube.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2f;
            _cube.SetActive(true);
        }
        else
        {
            _cube.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        if (MLHands.IsStarted)
        {
            MLHands.Stop();
        }
    }
}